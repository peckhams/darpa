# Livestock-model Overview

Livestock is vital to the economies of many developing countries. Animals are a source of food, more specifically protein for human diets, income, employment and possibly foreign exchange. For low income producers, livestock can serve as a store of wealth; provide draught power and organic fertilizer for crop production and a means of transport. Understanding spatial patterns of rangeland productivity is therefore crucial to designing national/regional development strategies that balance social and environmental benefits.

The Livestock model uses gridded climate and soils data to quantify the productivity of a rangeland area to support ruminant livestock populations. The model focuses on the impact of broad management decisions, such as stocking density and duration, on the viability of livestock production, using the metric of diet sufficiency. This metric describes, for each modeled time-step, the extent to which energy intake of the animal diet meets or exceeds maintenance energy needs.

## Input datasets
In order to run a crop model the following data are required:
- **Soil data**
    - Fraction sand, silt, clay at 20cm depth (%)
    - Bulk density at 20cm depth (g per cm³)
    - Soil pH at 20cm depth (pH scale)

Soil data used in the model were derived from [soil property maps of Africa at 250m resolution](https://www.isric.org/projects/soil-property-maps-africa-250-m-resolution).
- **Climate data**
    - Monthly average minimum daily temperature (ºC)
    - Monthly average maximum daily temperature (ºC)
    - Monthly precipitation (cm)

Climate data used in the model were derived from the [TerraClimate](http://www.climatologylab.org/terraclimate.html) database
- **Forage data**
    - Plant functional type (PFT) cover e.g., C3 grass, C4 grass, arid-shrubland
    - Proportion legume 
    - Management threshold (kg/ha)
    - Animal grazing areas (ha)

Plant functional type data was derived from [MODIS-MCD12Q1 annual land cover](https://doi.org/10.5067/MODIS/MCD12Q1.006) products while management threshold data was drived from litrature review.
- **Site initial conditions**
    - Site state variables table containing required values initial value for each site state variable
    - Plant functional type for herbaceous plants with their characteristics of C, N, and P in live and standing dead biomass.

Initial conditions data was drived from litrature review.
- **Livestock herd data** 
    - General breed (_Bos Indicus_ Cattle (Zebus), _Bos Taurus_ Cattle, Sheep, Goat) of grazing animal
    - Sex (non-castrated male, castrated male, breeding female, non-breeding female)
    - Average age (days)
    - Average weight (kg)
    - Weight at birth (kg)
    - Standard reference weight (kg)
    - Stocking density (animals per ha)
    - Conception month (for breeding females) (month relative to model starting month (may be negative; must be smaller than calving interval))
    - Calving interval (for breeding females) (months)
    - Lactation duration (for breeding females) (months)

Livestock data used in the model was derived from the Agricultural Sample Survey collected by the [Central Statistical Agency (CSA)](https://www.statsethiopia.gov.et/) of Ethiopia covering the period from 2003/04 to 2018/19. The standard reference weight was drived from from litrature review such as Freer et al. 2012. 

## The model
The model consists of a gridded (raster-based) implementation of the Century ecosystem model (Parton et al. 1993) coupled with a basic ruminant physiology sub-model adapted from GRAZPLAN (Freer et al. 2012). The Century sub-model simulates the growth of herbaceous forage according to climate and soil conditions at a monthly time-step. The ruminant physiology sub-model calculates off-take of forage by grazing animals according to the biomass and protein content of the simulated forage, and estimates the adequacy of the diet to meet the animals’ energy requirements. The estimated off-take by animals is integrated into the regrowth of forage according to Century’s existing grazing routine (Holland et al. 1992).
<div align='center'><fig><img src="Livestock_model_rpm.png"  width="50%" height="50%"><figcaption>Fig.1. The schematic diagram of livestock model. </figcaption></div>
<br>
The livestock herd is simulated as a static collection of age/sex classes, since uncertainty about the exact herd demographics over a landscape makes tracking the growth and consequent change in feeding requirements of an individual unrealistic. At each model step, the animal physiology sub-model calculates the maintenance energy and protein requirements of each livestock class; these reflect the animal’s age, weight, and general characteristics such as breed and normal size.  Because the energy requirements of breeding females fluctuate greatly during the reproductive cycle, the model tracks reproductive status and additional energy requirements due to pregnancy or lactation for these females.

## Output of the model
Livestock model output include:
* Potential Biomass (kg/ha). This is total potential modeled biomass in the absence of grazing, including live and standing dead fractions of all plant functional types. `luigi --module models.livestock_model.tasks models.livestock_model.tasks.PotentialBiomass --time 2015-01-01-2016-01-01 --local-scheduler --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --workers 20 --country-level Ethiopia`

* Standing biomass (kg/ha). This is total modeled biomass after offtake by grazing animals, including live and standing dead fractions of all plant functional types. `luigi --module models.livestock_model.tasks models.livestock_model.tasks.StandingBiomass --time 2015-01-01-2016-01-01 --local-scheduler --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --workers 20 --country-level Ethiopia`

* Animal Density. Distribution of animals inside grazing area polygons. `luigi --module models.livestock_model.tasks models.livestock_model.tasks.AnimalDensity --time 2015-01-01-2016-01-01 --local-scheduler --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --workers 20 --country-level Ethiopia`

* Diet sufficiency. Ratio of metabolizable energy intake to maintenance energy requirements on pixels where animals grazed. `luigi --module models.livestock_model.tasks models.livestock_model.tasks.DietSufficiency --time 2015-01-01-2016-01-01 --local-scheduler --geography /usr/src/app/models/geography/boundaries/ethiopia_2d.geojson --workers 20 --country-level Ethiopia`
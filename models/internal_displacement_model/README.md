# Internal Displacement Model

## Introduction 

According to the UNHCR, the global forced displacement surpassed  80 million people at the mid-2020 out of which over 46 million were internally displaced people (IDPs). It is also anticipated that the problem will grow in the foreseable future due to conflict, climate change and natural disasters. 

The massive displacement requires the allocation of emergency resources to the affected population which include shelter, food, and healthcare. Also, humanitarian organizations need to be prepared regarding feature displacement for effective aid allocation. As such, insights into the dynamics of  forced displacement over time and space has importance in supporting humanitarian and development decisions in affected countries. The current model allow users to predict internal displacement at one quarter a head of time and at admin two level in Ethiopia. 

## Target variable: Percent of displaced population in the next quarter

Predicting forced displacement has been challenging due to lack of data. To fill this gap, recent research focused on the use of artificial intelligence and remote sensing tools. For instance, Green and Blanford (2020) used aerial and satellite images to drive estimates of displaced populations in camps. Their work shows that high-resolution satellite imagery can be used to map physical structures in refugees and IDP camps, including changes to the number and type of these structures over time. The authors also indicated that the accuracy of automated tools rely on well-defined classifiers that are geographically and temporally transferable.

We use the displacement Site Assassment (SA) tracking data from IOM DTM(Displacement Matrix) that tracks the number and multisectoral needs of internally displaced persons (IDPs) on quarterly basis (approximation) to drive the historical data on displacement. 

The IDP stock in our case country, Ethiopia, has reached 1.96 million that are spread accross 1,222 sites in 11 regions by the end of January 2021 out of which conflict and drought displaced an estimate of 1.2 million (61%), and 400 thousand (20%) people, respectively (DTM, April 2021). 

Our focus is on the number newly displaced persons tracked in 24 rounds of quarterly surveys conducted from late 2017 to Jan 2021. To arrive at newly displaced persons per quarter, we took the difference of the number of displaced persons in the consecutive rounds of surveys. Then, the quarterly data was correlated with the lag of commmulative figures of input variables calculated on quarterly basis. Introducing lag in the time series enabled the model to forecast one quarter a head of time. 

## Conceptual Framework
We considere the influence of a range of factors in forcing people to flee their home. Here, we use a framework for understanding the effect of environmental migration at micro and macro level (Black et al., 2011) that draws on an increased evidence base on the topic.  The framework identifies five families of factors of migration decisions, namely; political, economic, social, demographic and environmental drivers. In the figure below, individual migration decisions and flows are affected by these drivers operating in combination. 

![](readme_images/mental_model.png)

## Final Input variables:

Following the above mental model, we tested a total of 17 factors which include political, environmental, demographic and socio-economic variables were considered for the emodel. Political indicators were mostly derived from the ACLED dataset by transforming the data to create new indicators. Other data sets have been accessed from different open sources. To this end, we arrived to the following 9 indicators after performing feature selection.

1. `drought_index`: mean evaporative stress index per quarter
2. `mean_rainfall`: average monthly rainfall per quarter
3. `settlement_trend`: mean settlement trends per year
4. `population_density`: mean number of people per KM2 per quarter
5. `peaceful_days`: mean duration of peaceful days before the next conflict incidence 
6. `ethnicity_count`: mean number of ethnic groups at admin two level
7. `youth_bulge`:proportion of youth population

## Mixed Effect Random Forest (MERF) Model

We employed Mixed Random Forest (MERF) algorithm. MERFs can be viewed as a method that combine the best of conventional linear mixed effects models with the power of machine learning in non-parametric modeling. Like Linear Mixed Effects (LME) which is a special case of a hierarchical Bayesian model, the MERF model assumes a generative model of the form:

![](readme_images/merf.png)

Where;

•	`y` is the target variable (proportion of displaced population).
•	`X` is the fixed effect features. X is assumed to be p dimensional, e.g. conflict_onset, population_density, etc.
•	`Z` is the random effect features (cluster or location variable). 
•	`e` is independent, identically distributed (iid) noise. It is distributed as N(0, sigma_e²)
•	`i` is the cluster index. We assume that there are k clusters in the training data.
•	`bi` is the random effect coefficients. They are different per cluster i but are assumed to be drawn from the same distribution. 

The unique feature of MERFs is its ability to model datasets with non-negligible random effects, or large differences by cluster such as country pair over time. Our dataset cluster column is represented by the admin names. The advantages of MERFs comes from the fact that the model controls for the variation at cluster level compared to modeling the whole dataset without including the cluster specific effects. This means that the model takes into accounnt the nature of displacement form specific locations that might exhibit certain differences compared to other countries. 

## How to run the model forecast?

To run the forecast in a default setting, use the following command:
```bash
luigi --module models.internal_displacement_model.tasks models.internal_displacement_model.tasks.Forecast\ 
--drought-index-scenario 0 --fatalities-per-event-scenario 0 --local-scheduler
```
Changes in model parameters (`fatalities_per_event` and `drought_index`) can be introduced as follows:

```bash
luigi --module models.internal_displacement_model.tasks models.internal_displacement_model.tasks.Forecast \
--drought-index-scenario i --fatalities-per-event-scenario i --local-scheduler
```
 where `i` is value ranging from -1 to 1. `i`<0 implies that we are reducing the impact of the default parameter while the reverse is true when `i`>0.
 
 Changes in model parameters (`ethnicity_count` and `peaceful_days`) can be introduced as follows:

```bash
luigi --module models.internal_displacement_model.tasks models.internal_displacement_model.tasks.Forecast \
--ethnicity-count i --peaceful-days i --local-scheduler
```
 where `i` is an intiger value >/< 0.
# This file holds deprecated functions no longer in use


# y_data_prep.py

# from .mappings import DHS_2016_toilet_facility_map
#
#
# @inherits(GlobalParameters)
# class DHSClusterGPS(ExternalTask):
#     def output(self):
#         if self.country_level == "Ethiopia":
#             return LocalTarget(path="output/DHS_GPS/ETGE71FL.ZIP")
#         else:
#             return NotImplementedError
#
#
# @inherits(GlobalParameters)
# class DHSWASHSurvey(ExternalTask):
#     def output(self):
#         if self.country_level == "Ethiopia":
#             return CkanTarget(
#                 dataset={"id": "353a8727-c58c-4aba-93e8-eb8081f21c18"},
#                 resource={"id": "575ed664-c291-4780-b971-1b885cf9e568"},
#             )
#         else:
#             breakpoint()
#             raise NotImplementedError
#
#
# @requires(DHSClusterGPS, DHSWASHSurvey)
# class DHSWASHData(Task):
#     def output(self):
#         return IntermediateTarget(task=self, timeout=31536000)
#
#     def run(self):
#         gdf = gpd.read_file(f"zip://{self.input()[0].path}")
#         df = read_data(self.input()[1].path)
#         df = df.drop_duplicates(subset=["hhid"])
#         df["sanitation"] = df["hv205"].replace(DHS_2016_toilet_facility_map)
#         df = df.loc[df["sanitation"] != "Other"]
#         df.loc[
#             (df["sanitation"] == "At least basic") & (df["hv225"] == "yes"),
#             "sanitation",
#         ] = "Unimproved"
#         df["unimproved_sanitation"] = 0
#         df.loc[df["sanitation"] != "At least basic", "unimproved_sanitation"] = 1
#         df["Total"] = 1
#         df["month"] = df.apply(
#             lambda x: to_gregorian(x["hv007"], x["hv006"], 1), axis=1
#         )
#         df["month"] = pd.to_datetime(df["month"])
#         df["year"] = df["month"].dt.year
#         df["month_num"] = df["month"].dt.month
#         df = df.rename(columns={"hv001": "DHSCLUST"})
#         df = df.groupby(["DHSCLUST", "year", "month_num"], as_index=False)[
#             "unimproved_sanitation", "Total"
#         ].sum()
#         df["unimproved_sanitation"] = df["unimproved_sanitation"] / df["Total"]
#         df = df.drop("Total", axis=1)
#         df = df.merge(gdf, on="DHSCLUST", how="left")
#         with self.output().open("w") as out:
#             out.write(df)
#
#
# @requires(DHSWASHData, LSMSWASHData)
# class DHSAndLSMSData(Task):
#     def output(self):
#         return IntermediateTarget(task=self, timeout=31536000)
#
#     def run(self):
#         with self.input()[0].open() as src:
#             df_dhs = src.read()
#
#         df_dhs = df_dhs.rename(columns={"LATNUM": "latitude", "LONGNUM": "longitude"})
#         df_dhs = gpd.GeoDataFrame(
#             df_dhs, geometry=gpd.points_from_xy(df_dhs["longitude"], df_dhs["latitude"])
#         )
#         df_dhs = df_dhs.set_crs(epsg=4326)
#         with self.input()[1].open() as src:
#             df_lsms = src.read()
#
#         df_lsms = df_lsms.rename(
#             columns={"lat_dd_mod": "latitude", "lon_dd_mod": "longitude"}
#         )
#         df_lsms = gpd.GeoDataFrame(
#             df_lsms,
#             geometry=gpd.points_from_xy(df_lsms["longitude"], df_lsms["latitude"]),
#         )
#         df_lsms = df_lsms.set_crs(epsg=4326)
#         out_df = pd.concat([df_dhs, df_lsms])
#         out_df = out_df.reset_index(drop=True)
#         with self.output().open("w") as out:
#             out.write(out_df)

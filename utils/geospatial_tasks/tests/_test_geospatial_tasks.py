import json
import os
import pickle  # noqa: S403

import luigi
from luigi import ExternalTask, LocalTarget
from luigi.util import requires
from utils.geospatial_tasks.luigi_wrappers import (
    Array2Raster,
    BufferPoints,
    GetRasterProj,
    GetRasterProjConfigFile,
    MaskRaster,
    Raster2Array,
    RasterizeVectorFile,
)

import numpy as np


# -----------
# Luigi Tasks
# -----------
# Raster projection data
class ExternalRasterFile(ExternalTask):
    def output(self):
        root_dir = "tests"
        return LocalTarget(os.path.join(root_dir, "test_raster.tif"))


class ExternalSPEIFile(ExternalTask):
    def output(self):
        root_dir = "tests"
        return LocalTarget(os.path.join(root_dir, "SPEI_07_2016.tif"))


class ExternalRasterFile2(ExternalTask):
    def output(self):
        root_dir = "tests"
        return LocalTarget(os.path.join(root_dir, "population_age_65-69.tif"))


class ExternalTempRasterFile(ExternalTask):
    def output(self):
        root_dir = "."
        return LocalTarget(os.path.join(root_dir, "out.tif"))


class ExternalTemp2RasterFile(ExternalTask):
    def output(self):
        root_dir = "tests"
        return LocalTarget(os.path.join(root_dir, "test_temp_raster.tif"))


class ExternalVectorFile(ExternalTask):
    def output(self):
        root_dir = "tests"
        return LocalTarget(os.path.join(root_dir, "population_age_65-69.shp"))


class ExternalConfigFile(ExternalTask):
    def output(self):
        root_dir = "tests"
        return LocalTarget(os.path.join(root_dir, "raster_config.ini"))


class ExternalLocFile(ExternalTask):
    def output(self):
        root_dir = "tests"
        return LocalTarget(os.path.join(root_dir, "ethiopia_cluster.csv"))


# Projection from Raster file
@requires(ExternalRasterFile)
class PopExpProjection(GetRasterProj):
    pass


# Projection from Config file
@requires(ExternalConfigFile)
class PopExpProjectionFromConfig(GetRasterProjConfigFile):
    pass


# raster2array
@requires(ExternalRasterFile)
class ConvertRaster2Array(Raster2Array):
    pass


@requires(ExternalRasterFile2)
class ConvertRaster2Array2(Raster2Array):
    pass


@requires(ExternalTempRasterFile)
class ConvertTempRaster2Array(Raster2Array):
    pass


# array2raster
@requires(PopExpProjectionFromConfig, ConvertRaster2Array)
class ConvertArray2Raster(Array2Raster):
    pass


# vector2raster
@requires(PopExpProjectionFromConfig, ExternalVectorFile)
class ConvertVector2Raster(RasterizeVectorFile):
    pass


@requires(ExternalRasterFile, ExternalLocFile)
class BufferEthiopiaPoints(BufferPoints):
    pass


@requires(ExternalSPEIFile, BufferEthiopiaPoints)
class MaskEthiopiaPoints(MaskRaster):
    pass


# ------------
# Unit testing
# ------------
def test_raster_proj():

    # Luigi task dataframe should be:
    rpj_ideal = {
        "srs": (
            'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0],UNIT["degree",'
            '0.0174532925199433],AUTHORITY["EPSG","4326"]]'
        ),
        "pixel": [
            24.150733709000065,
            0.03333669445197738,
            0,
            12.23635188000003,
            0,
            -0.033293003357414364,
        ],
        "ncolsrows": [354, 263],
    }

    # Build Luigi Pipeline for extracting raster projection
    luigi.build([PopExpProjection()])

    PopExpProjection().run()
    task = PopExpProjection()
    with task.output().open() as fid:
        rpj_out = json.load(fid)

    assert rpj_out == rpj_ideal


def test_raster_proj_from_config():

    expected = {
        "srs": (
            r'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0],UNIT["degree",'
            r'0.0174532925199433],AUTHORITY["EPSG","4326"]]'
        ),
        "wkp": "EPSG:4326",
        "pixel": [
            24.150733709000065,
            0.03333669445197738,
            0,
            12.23635188000003,
            0,
            -0.033293003357414364,
        ],
        "ncolsrows": [354, 263],
    }
    # Build Luigi Pipeline for extracting raster projection
    luigi.build([PopExpProjectionFromConfig()])

    PopExpProjectionFromConfig().run()
    task = PopExpProjectionFromConfig()
    with task.output().open() as fid:
        actual = json.load(fid)

    assert expected == actual


def test_raster2array():

    expected = np.float32(104.972)
    # Build Luigi Pipeline for extracting raster projection
    luigi.build([ConvertRaster2Array()])

    ConvertRaster2Array().run()
    task = ConvertRaster2Array()
    with task.output().open() as fid:
        actual = pickle.load(fid)  # noqa: S301

    assert round(expected, 3) == round(actual[100, 100], 3)


def test_array2raster():

    luigi.build(
        [ConvertArray2Raster(), ConvertTempRaster2Array(), ConvertRaster2Array2()]
    )

    ConvertArray2Raster().run()
    ConvertTempRaster2Array().run()
    ConvertRaster2Array().run()

    task_expected = ConvertTempRaster2Array()
    task_actual = ConvertRaster2Array()

    with task_expected.output().open() as fid:
        expected = pickle.load(fid)  # noqa: S301

    with task_actual.output().open() as fid:
        actual = pickle.load(fid)  # noqa: S301

    assert np.array_equal(expected, actual)


def test_vector2raster():

    luigi.build(
        [
            ConvertVector2Raster(attr_field="Percent Bo"),
            ConvertRaster2Array2(),
            ConvertTempRaster2Array(),
        ]
    )

    ConvertVector2Raster(attr_field="Percent Bo").run()

    task_expected = ConvertTempRaster2Array()
    task_actual = ConvertRaster2Array2()

    with task_expected.output().open() as fid:
        expected = pickle.load(fid)  # noqa: S301

    with task_actual.output().open() as fid:
        actual = pickle.load(fid)  # noqa: S301

    assert np.array_equal(expected, actual)


def test_bufferpoints():

    luigi.build([BufferEthiopiaPoints()])

    BufferEthiopiaPoints().run()

    task_expected = BufferEthiopiaPoints()

    assert os.path.isfile(task_expected.output().path)


def test_maskbuffer():

    luigi.build([MaskEthiopiaPoints()])

    MaskEthiopiaPoints().run()

    task_expected = MaskEthiopiaPoints()

    assert os.path.isfile(task_expected.output().path)
